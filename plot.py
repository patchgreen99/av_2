import matplotlib.pyplot as plt
import numpy as np

datax = np.load("results/results_planex.npy")
imgx = plt.figure(num="RMS vs X scene position")
plt.scatter(datax[:,0], datax[:,-1])
plt.ylim([0, np.max(datax[:,-1])])
imgx.show()

datay = np.load("results/results_planey.npy")
imgy = plt.figure(num="RMS vs Y scene position")
plt.scatter(datay[:,1], datay[:,-1])
plt.ylim([0, np.max(datay[:,-1])])
imgy.show()

dataz = np.load("results/results_planez.npy")
imgz = plt.figure(num="RMS vs average Z")
plt.scatter(dataz[:,0], dataz[:,-1])
plt.ylim([0, np.max(dataz[:,-1])])
imgz.show()

data = np.load("results/results_plane.npy")
img = plt.figure(num="RMS vs average Z - static")
plt.scatter(data[:,0], data[:,-1])
plt.ylim([0, np.max(data[:,-1])])
img.show()

plt.show(block=True)
