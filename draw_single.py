import scipy.io
import cv2
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from lib.match import fitplane_m
from matplotlib import colors

plt.close("all")

s = np.array([400, 200]) # top left of patch
w = np.array([500, 500]) # col and row count of patch
e = s+w                  # bottom right of patch

mat = scipy.io.loadmat('data/planez_16.mat')

# create data array
XYZ = np.asarray(mat['XYZ'])
Img = np.asarray(mat['Img'])
m = XYZ.shape[0]
n = XYZ.shape[1]
data = np.zeros((m,n,6))
X = XYZ[:,:,0]
Y = XYZ[:,:,1]
Z = XYZ[:,:,2]
K = np.isnan(X) | np.isinf(X) | np.isnan(Y) | np.isinf(Y) | np.isnan(Z) | np.isinf(Z)
X[K] = 0
Y[K] = 0
Z[K] = 0
data[:, :, 0] = Img[:,:]
data[:, :, 1] = Img[:,:]
data[:, :, 2] = Img[:,:]
data[:, :, 3] = X
data[:, :, 4] = Y
data[:, :, 5] = Z
# data done

# grey scale image anyway so just call first indices
frame = data[:, :, 0] * 255.0

# add patch box
cv2.rectangle(frame, tuple(s), tuple(e), (0, 0, 255), 5)

# plot the base image + patch box
img = plt.figure(num="Base image")
plt.imshow(frame, cmap = 'gray')
plt.xlabel('X')
plt.ylabel('Y')
img.show()

# get X and Y coords for mesh drawing
X_m = np.arange(m)
Y_m = np.arange(n)
X_m, Y_m = np.meshgrid(X_m, Y_m)

'''
# plot 3D mesh
mesh = plt.figure(num="Entire 3D plot")
ax = mesh.gca(projection='3d')
ax.plot_surface(X_m, Y_m, Z, linewidth=0.2, antialiased=True)
plt.xlabel('X')
plt.ylabel('Y')
ax.set_zlabel('Z')
mesh.show()
'''

# extract patch points
patch = data[s[1]:e[1],s[0]:e[0],3:]
X_p = patch[:,:,0]
Y_p = patch[:,:,1]
Z_p = patch[:,:,2]

# plot X component
imgx = plt.figure(num="Patch X component")
plt.imshow(X_p, cmap = 'gray')
plt.xlabel('X')
plt.ylabel('Y')
plt.colorbar()
imgx.show()

# plot Y component
imgy = plt.figure(num="Patch Y component")
plt.imshow(Y_p, cmap = 'gray')
plt.xlabel('X')
plt.ylabel('Y')
plt.colorbar()
imgy.show()

# plot Z component
imgz = plt.figure(num="Patch Z component")
plt.imshow(Z_p, cmap = 'gray')
plt.xlabel('X')
plt.ylabel('Y')
plt.colorbar()
imgz.show()

# get X and Y coords for patch mesh drawing
X_m = np.arange(s[0],e[0])
Y_m = np.arange(s[1],e[1])
X_m, Y_m = np.meshgrid(X_m, Y_m)

'''
# plot 3D patch
patch_mesh = plt.figure(num="Patch 3D plot")
ax = patch_mesh.gca(projection='3d')
ax.plot_surface(X_m, Y_m, Z_p, linewidth=0.2, antialiased=True)
plt.xlabel('X')
plt.ylabel('Y')
ax.set_zlabel('Z')
patch_mesh.show()
'''

# calculate plane equation, rms, residuals
C, rms, residuals = fitplane_m(patch)
Z = C[0]*X_p + C[1]*Y_p + C[2]
print "RMS is {}".format(rms)

'''
# plot fitted plane
plane = plt.figure(num="Fitted plane")
ax = plane.gca(projection='3d')
ax.plot_surface(X_m, Y_m, Z, linewidth=0.2, antialiased=True)
plt.xlabel('X')
plt.ylabel('Y')
ax.set_zlabel('Z')
plane.show()
'''

# build the color map for residuals
heatmat = np.copy(residuals)
cmap = colors.ListedColormap(['red','yellow','green','blue'])
bounds=[np.min(residuals),-rms,0,rms,np.max(residuals)]
norm = colors.BoundaryNorm(bounds, cmap.N)

# plot residual color map
error = plt.figure(num="Errors")
img = plt.imshow(residuals, interpolation='nearest', origin='upper', cmap=cmap, norm=norm)
plt.colorbar(img, cmap=cmap, norm=norm, boundaries=bounds, ticks=[np.min(residuals),-rms,0,rms,np.max(residuals)])

plt.show(block=True)
