import scipy.io
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import glob
import re


def load_mesh(name):
    for frame in sorted(glob.glob("data/*.mat")):
        if re.sub("^[^\/]+\/|_[0-9]*\.mat", "", frame) == name:
            mat = scipy.io.loadmat(frame)
            XYZ = np.asarray(mat['XYZ'])
            Img = np.asarray(mat['Img'])

            m = XYZ.shape[0]
            n = XYZ.shape[1]
            RGBXYZ = np.zeros((m,n,6))

            X = XYZ[:,:,0]
            Y = XYZ[:,:,1]
            Z = XYZ[:,:,2]
            K = np.isnan(X) | np.isinf(X) | np.isnan(Y) | np.isinf(Y) | np.isnan(Z) | np.isinf(Z)

            X[K] = 0
            Y[K] = 0
            Z[K] = 0

            RGBXYZ[:, :, 0] = Img[:,:]
            RGBXYZ[:, :, 1] = Img[:,:]
            RGBXYZ[:, :, 2] = Img[:,:]
            RGBXYZ[:, :, 3] = X
            RGBXYZ[:, :, 4] = Y
            RGBXYZ[:, :, 5] = Z

            yield RGBXYZ
